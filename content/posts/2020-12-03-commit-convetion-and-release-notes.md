---
title: "Apply commit convention and generate release notes automatically"
date: "2020-12-03"
tags: ["happy-coding"]
aliases: ["commit-convention"]
ShowToc: true
TocOpen: true
weight: 2
---

Hello, we start the post from the reason first.

## Problem

You know a project has many developers contribute to it.

When projects has no convention for commit messages then each developer can make commit message in their own styles.

This is a commit log you may see.

![unconvention](../static/201203-no-commit-convention.png)

From this log, it is pretty hard to know what changed in a commit, category of that change, it is a feature or a fix,..

Then, you have to go to details of each commit, see which file changed to understand commit's purpose.

And, when you create a release note, you have to check the commit list and write the note by yourself,
because commit messages do not follow a format to automatically generate.

## Solution

We need a commit convention for projects.
One of good conventions is [Commit Message Guidelines](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines) by angular.

In breif, the message format is

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

where type should be one of

```
build: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
ci: Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)
docs: Documentation only changes
feat: A new feature
fix: A bug fix
perf: A code change that improves performance
refactor: A code change that neither fixes a bug nor adds a feature
style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
test: Adding missing tests or correcting existing tests
```

### husky

We had the convention, but we also need a way to force every commits follow this convertion.

In order to do some checks when a commit is made, we use [husky](https://www.npmjs.com/package/husky).

![husky](../static/201203-husky.jpg)

(Source [needpix.com](https://www.needpix.com/photo/1858537/husky-siberian-husky-dog-pet-siberian-animal-pets-puppy-cute))

Basically, `husky` use [git hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) to configure what need to do to a git hook.

The git hook we need in this case is `commit-msg`.

### commitlint

We had the git hook, we need a tool that check commit message for us.

And, that tool is [commitlint](https://commitlint.js.org/).

Combine these two ones, we have

```
// File: .huskyrc
{
    "hooks": {
        "commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
    }
}
```

```
// File: .commitlintrc.js
module.exports = {extends: ['@commitlint/config-conventional']}
```

Yay, we had a guard that prevents bad commit messages.

![commitlint-test](../static/201203-commitlint-test.png)

### git-cz

But, life can be easier if we have a form to enter commit info then it makes good commmit message for us.

Here [git-cz](https://www.npmjs.com/package/git-cz) comes.

What you need to do here is setting it in package.json.

```
// File: package.json
  "scripts": {
    ...
    "commit": "git-cz",
  },
```

and

```
// File: .czrc
{
  "path": "cz-conventional-changelog"
}
```

Then, you could run

![commitlint-test](../static/201203-git-cz.png)

### standard-version

It is really good after we have all commit messages following a convention.

Now, we can use [standard-version](https://github.com/conventional-changelog/standard-version) to generate release notes automatically.

```
// File: package.json
  "scripts": {
    ...
    "release": "standard-version",
  },
```

And, you can configure which categories appear in release notes, like this

```
// File: .versionrc
{
  "types": [
    {"type": "chore", "section":"Others", "hidden": false},
    {"type": "revert", "section":"Reverts", "hidden": false},
    {"type": "feat", "section": "Features", "hidden": false},
    {"type": "fix", "section": "Bug Fixes", "hidden": false},
    {"type": "improvement", "section": "Feature Improvements", "hidden": false},
    {"type": "docs", "section":"Docs", "hidden": false},
    {"type": "style", "section":"Styling", "hidden": false},
    {"type": "refactor", "section":"Code Refactoring", "hidden": false},
    {"type": "perf", "section":"Performance Improvements", "hidden": false},
    {"type": "test", "section":"Tests", "hidden": false},
    {"type": "build", "section":"Build System", "hidden": false},
    {"type": "ci", "section":"CI", "hidden":false}
  ]
}
```

This is result

![npm run release](../static/201203-release.png)

![changelog](../static/201203-changelog.png)

Yay, everything completed.

You can check the sample project [here](https://gitlab.com/tqquan-sample/commit-convention).
