---
title: "Notes"
layout: "notes"
url: "/notes"
summary: "notes"
---

**Some good articles:**

- [The Wrong Abstraction](https://sandimetz.com/blog/2016/1/20/the-wrong-abstraction)

    If you find yourself passing parameters and adding conditional paths through shared code, the abstraction is incorrect.